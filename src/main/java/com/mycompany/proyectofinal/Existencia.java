/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.proyectofinal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author alan
 */
public class Existencia {
    String nombre;
    String dni;
    public Existencia(){
        nombre = "";
        dni = "";
    }
    public static String CompruebaUsuario() throws SQLException{
        Connection cn =Conexion.GetConnection();
        Conexion c = new Conexion();
        String nombre_alumno = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = cn.prepareStatement("SELECT Nombre FROM RegistroUsuario WHERE id = ?");
            ps.setInt(1, 1);
            rs = ps.executeQuery();
            while (rs.next()) {                
                nombre_alumno = rs.getString("Nombre");
                System.out.println("Nombre: "+nombre_alumno);
            } 
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e, "error en la conexion"+ e.getMessage(),JOptionPane.ERROR_MESSAGE);
        } return nombre_alumno; 
    } 

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    public static String CompruebaContraseña() throws SQLException{
        Connection cn =Conexion.GetConnection();
        String contraseña = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = cn.prepareStatement("SELECT DNI FROM RegistroUsuario WHERE id = ?");
            ps.setInt(1, 1);
            rs = ps.executeQuery();
            while (rs.next()) {                
                contraseña = rs.getString("DNI");
                System.out.println("Contrasenia: "+contraseña);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e, "error en la conexion"+ e.getMessage(),JOptionPane.ERROR_MESSAGE);
        } return contraseña;
      
    }
    //public void CompruebaContraseña(){      
    //}
  
}
