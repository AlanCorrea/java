/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.proyectofinal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import javax.swing.JOptionPane;


public class Mostrar {
    String consulta;
    public Mostrar(){
        consulta ="";
    }
    public ResultSet Select () throws SQLException {
        Connection cn =Conexion.GetConnection();
        //String nombre_alumno = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        //Vector v = new Vector (10,1);
         try {
            ps = cn.prepareStatement(getConsulta());
            //ps.setInt(1, 2);
            rs = ps.executeQuery();
            /*while (rs.next()) {                
                //nombre_alumno = rs.getString("nombre");
                //System.out.println("Nombre: "+nombre_alumno);
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
 
            }*/
            /*for (int i=0;i<v.size();i++){
                
                System.out.println("Elemento: "+v.elementAt(i));
              
        } */
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e, "error en la conexion"+ e.getMessage(),JOptionPane.ERROR_MESSAGE);
        } return rs;
    }
    public void Update() throws SQLException {
        Connection cn =Conexion.GetConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = cn.prepareStatement(getConsulta());
            //ps.setInt(1, 2);
            ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e, "error en la conexion"+ e.getMessage(),JOptionPane.ERROR_MESSAGE);
    }
    }
     public void setConsulta(String consulta_A){
         consulta  = consulta_A;
     }
     public String getConsulta(){
         return consulta;
     }

    void setVisible(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
