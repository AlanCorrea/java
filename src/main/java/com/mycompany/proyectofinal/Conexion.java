/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.proyectofinal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;


public class Conexion {
    public static Connection conexion;
public Conexion(){
    conexion = null;
}
    public static Connection GetConnection() throws SQLException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String servidor = "jdbc:mysql://localhost/BolsaDeTrabajo2";
            String usuarioDB="alan";
            String passwordDB="admin";
            conexion= DriverManager.getConnection(servidor,usuarioDB,passwordDB);
        }
        catch(ClassNotFoundException   ex){
            JOptionPane.showMessageDialog(null, ex, "error en la conexion"+ ex.getMessage(),JOptionPane.ERROR_MESSAGE);
            conexion=null;}return conexion;
    }
    public static Connection closecConnection() throws SQLException{
        try {
            conexion.close();
        } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, ex, "error de desconexion"+ ex.getMessage(),JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    
}

